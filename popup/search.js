window.addEventListener("load", function() {
	var search = document.getElementById("search");
    var site = document.getElementById("site");
	search.focus();
	search.addEventListener("keypress", function(e) {
		if(this.value.indexOf(".") !== -1)
			if(e.keyCode === 13 || e.which === 13)
				location.href = "site.html?site=" + this.value;
	});
	function updateText() {
		if(this.value.trim().length > 0 && this.value.indexOf(".") !== -1) {
			site.parentNode.style.display = 'block';
			site.parentNode.href = "site.html?site=" + encodeURI(this.value);
			site.innerHTML = "";
			site.appendChild(document.createTextNode(this.value.toLowerCase()));
		} else {
			site.parentNode.style.display = 'none';
		}
	}
	search.addEventListener("input", updateText);

	document.getElementById("cancel").addEventListener("click", function() {
		search.value = '';
		updateText.call(search);
		search.focus();
	});
	
});