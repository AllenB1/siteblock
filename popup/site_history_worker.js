function parseHistory(obj) {
	var data = [];
	var labels = [];
	var entries = Object.entries(obj);
	entries.sort(function(a, b) {
		var thingsA = /(\d{1,})\/(\d{1,})/.exec(a);
		thingsA.shift();
		var thingsB = /(\d{1,})\/(\d{1,})/.exec(b);
		thingsB.shift();
	
		var dateA = new Date(thingsA[1] | 0, thingsA[0] | 0).getTime();
		var dateB = new Date(thingsB[1] | 0, thingsB[0] | 0).getTime();
		return dateA - dateB;
	});
	entries.forEach(function(entry) {
		var things = /(\d{1,})\/(\d{1,})/.exec(entry[0]);
		things.shift();
		var date = new Date(things[1] | 0, things[0] | 0);
		labels.push(date.toLocaleDateString('en', {year: 'numeric', month: 'short'}));
		data.push({
			x: date.getTime()  / (1000 * 60 * 60 * 24), 
			y: entry[1]
		});
	});
	return {data: data, labels: labels};
}

self.onmessage = function(e) {
	self.postMessage(parseHistory(e.data));
}