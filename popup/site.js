const ext = (window.browser || window.chrome);

var url_params;
(window.onpopstate = function () {
	var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);

	url_params = {};
	while (match = search.exec(query))
	url_params[decode(match[1])] = decode(match[2]);
})();

function parseHistory(obj) {
	return new Promise(function(resolve) {
		var worker = new Worker("site_history_worker.js");
		worker.postMessage(obj);
		worker.onmessage = function(e) {
			resolve(e.data);
		};
	});
}

// fetches history and returns the total number of vists and the distribution of those visits over the different months
function getHistory(site) {
	var gotVisits = function(visits) {
		var visitsOverTime = {};
		visits.forEach(function(visit) {
			var date = new Date(visit.visitTime);
			var str = date.getMonth() + "/" + date.getFullYear();
			if(str in visitsOverTime)
				visitsOverTime[str] += 1;
			else
				visitsOverTime[str] = 1;
		});
		return visitsOverTime;
	}

	var cb = function(items) {
		var totalVisits = 0;

		var promises = [];

		items.forEach(function(item) {
			if(item.url) {
				var url = new URL(item.url);
				if(url.host === site) {
					totalVisits += item.visitCount;
					if(self.browser)
						promises.push(browser.history.getVisits({url:item.url}).then(gotVisits));
					else
						promises.push(new Promise(function(resolve) {
							chrome.history.getVisits({url:item.url}, function(x) { resolve(gotVisits(x)); });
						}));
				}
			}
		});

		return Promise.all(promises).then(function(results) {
			var visitsOverTime = {};
			results.forEach(function(res) {
				for(var date in res) {
					visitsOverTime[date] = (visitsOverTime[date] | 0) + res[date];
				}
			});

			return {total: totalVisits, results: visitsOverTime};
		});
	}

	if(self.browser)
		return browser.history.search({text: site, startTime: 0, maxResults: (Math.abs(1 << 31) - 1)}).then(cb);
	else if(self.chrome)
		return new Promise(function(resolve) {
			chrome.history.search({text: site, startTime: 0, maxResults: (Math.abs(1 << 31) - 1)}, function(items) {
				resolve(cb(items));
			});
		});
}


window.addEventListener("load", function() {
	var site = url_params.site;

	var title = document.getElementById("title");
	title.innerHTML = "";
	title.appendChild(document.createTextNode(site));

	var visit = document.getElementById("visit");
	visit.onclick = function() {
		ext.tabs.create({url: "http://" + site});	
	}

	var block = document.getElementById("block");
	function updateMessage(blocked) {
		block.innerHTML = blocked ? "Unblock" : "Block";
	}
	siteblock.block.isBlocked("http://" + site).then(updateMessage);

	block.addEventListener("click", function() {
		siteblock.block.isBlocked("http://" + site).then(function(blocked) {
			if(blocked)
				if(true) {
					siteblock.block.unblock("http://" + site).then(function() {
					updateMessage(false);
				});
				} else
					console.log("denied");
			else {
				siteblock.block.block("http://" + site).then(function() {
					updateMessage(true);
				});
			}
		});
	});

	getHistory(site).then(function(obj) {
		document.getElementById("clicks").innerHTML = obj.total | 0; // just double-checking
		parseHistory(obj.results).then(function(res) {
			var ctx = document.getElementById("canvas");

			var chart = new Chart(ctx, {
				type: 'line',	
				data: {
					labels: res.labels,
					datasets: [{
						label: "Visits",
						backgroundColor: "rgba(0,0,0,0)",
						data: res.data
					}]
				},
				options: {
					title: { display: true,
						fontFamily: "Roboto, sans-serif",
						fontSize: 15,
						fontStyle: 'normal',	
						text: "Activity"
					},
					legend: {display: false} /* only one dataset, not needed */
				}
			});

		});
	});
});
