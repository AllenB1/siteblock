/* Web worker */

self.onmessage = function(e) {
	var data = [];
	for(site in e.data) {
		data.push([site, e.data[site]]);
	}

	data.sort(function(a, b) {
		if(a[1] > b[1])
			return -1;
		else if(a[1] === b[1])
			return 0;
		else
			return 1;
	});

	/* Other *
	var limit = data[Math.floor(data.length / 4)] ? data[Math.floor(data.length / 4)][1] : 500;

	// Algorithm not working
	var other = 0;
	data.forEach(function(item) {
		if(item[1] <= limit)
			other += item[1];
	});
	data = data.filter(function(item) { return item[1] > limit && item[0].indexOf(".") !== -1; //make sure it's an actual url
		});
	data.push(["Other", other]);
	/* Endother */

	var sites = data.map(function(arr) { return arr[0]});
	var datapts = data.map(function(arr) { return arr[1]});

	self.postMessage({sites: sites, data: datapts});
}
