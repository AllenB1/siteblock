var ext = (self.browser || self.chrome);

window.addEventListener("load", function() {
/*    function updateTime() {
        ext.storage.local.get("time", function(res) {
            var total_mil = (Math.abs(Date.now() - res.time.lastStart) + res.time.duration);
            var total_sec = Math.floor(total_mil / 1000);
            var min = Math.floor(total_sec / 60);
            var sec = total_sec % 60;
            var strsec = (sec >= 10 ? sec : ("0" + sec))
            document.getElementById("p").innerHTML = min + ":" + strsec;
        });
    }
    setInterval(updateTime, 1000);
    updateTime(); */

    var chart;

    /* history */
    var history_worker = new Worker("history_worker.js");

    function displayChart(res, startTime) {
        /* NOTE: ugly workaround, but I haven't found a better solution (startTime changes on the onClick callback) */
        nowStartTime = startTime;

        /* Colors */
        var allcolors = ["#f44336", "#ffc107", "#8bc34a", "#00bcd4", "#3f51b5", "#9c27b0"];
        var colors = [];
        for(var i = 0; i < res.sites.length; i++) {
            if(res.sites[i] !== "Other")
                colors.push(allcolors[i % allcolors.length]);
            else
                colors.push("#dddddd");
        }

        var today = new Date();
        if(!chart) {
            var ctx = document.getElementById("chart-canvas").getContext('2d');
            chart = new Chart(ctx,{
                type: ((today.getMonth() === 2) && (today.getDate() === 14) ? 'pie' : 'doughnut'),
                data: {
                    datasets: [{
                        data: res.data,
                        backgroundColor: colors
                    }],
                    labels: res.sites
                },
                options: {
                    legend: {
                        display: false },
                    title: {
                        display: false },
                    onClick: function(evt, elem) {
                        try {
                            var site = elem[0]._model.label;
                            if(site === "Other") {
                                ext.tabs.create({url: "about:history"});
                            } else {
                                location.href = ext.runtime.getURL("/popup/site.html?site=" + site + "&time=" + nowStartTime);
                            }
                        } catch(err) {}
                    }
                }
            });
        } else {
            chart.data.labels = res.sites;
            chart.data.datasets = [{data:res.data,backgroundColor:colors}];
            chart.update();
        }
    }

    function getHistory(startTime) {
        var cb = function(items) {
            var obj = {};

            /* Loop through all visited sites */
            items.forEach(function(item, index) {
                try {
                    var url = new URL(item.url);
                    if(url.host === "" || url.host.indexOf('.') === -1)
                        return;

                    /* Find number of visits */
                    if(typeof obj[url.host] === "number") {
                        obj[url.host] += item.visitCount;
                    } else {
                        obj[url.host] = item.visitCount;
                    }
                } catch(err) {
                    return;
                }
            });

            return obj;
        };

        if(self.browser) return browser.history.search({text:"",startTime: startTime, maxResults: (Math.abs(1 << 31) - 1)}).then(cb);
        else return new Promise(function(resolve) {
            ext.history.search({text:"",startTime: startTime, maxResults: (Math.abs(1 << 31) - 1)}, function(items) {
                resolve(cb(items));
            });
        });
    }

    function loadHistory(startTime) {
        var ext = (self.browser || self.chrome);

        getHistory(startTime).then(function(obj) {
            history_worker.onmessage = function(e) {
                displayChart(e.data, startTime);
            };
            history_worker.postMessage(obj);
        });
    }

    var from = document.getElementById("from");
    from.onchange = function()  {
        switch(this.options[from.selectedIndex].value) {
            case "last-week":
                loadHistory(Date.now() - (7 * 24 * 60 * 60 * 1000));
                break;
            case "forever":
                loadHistory(0);
                break;
            case "two-days":
                loadHistory(Date.now() - (24 * 60 * 60 * 1000));
            case "today":
                var now = new Date();
                loadHistory(new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime(), "today");
        }
    }
    from.onchange();

/*    document.getElementById("menu").onclick = function() {
        mcss.openDrawer(document.getElementById("drawer"));
    }; */
});
