/* Inject script */
var ext = (window.browser || window.chrome);

var params = new URLSearchParams(location.search);

var site = params.get("site");
var text = params.get("text");

window.addEventListener("load", function() {
	var msg = document.getElementById("msg");
    msg.innerHTML = "";
	msg.appendChild(document.createTextNode(text ? text : site + " is blocked"));

    var edit = document.getElementById("edit");
    if(window.browser && browser.browserAction.openPopup) {
        browser.tabs.getCurrent().then(function(tabInfo) {
            edit.addEventListener("click", function() {
                if(window.browser) {
                    browser.browserAction.setPopup({
                        tabId: tabInfo.id,
                        popup: "/popup/site.html?site=" + site});
                    browser.browserAction.openPopup();
                    /* for compatability */
                    browser.browserAction.setPopup({
                        tabId: tabInfo.id,
                        popup: "/popup/popup.html"});

                    browser.browserAction.setPopup({
                        tabId: tabInfo.id,
                        popup: null});
                }
            });
        });
    } else {
        edit.style.display = 'none';
    }

    var reload = document.getElementById("reload");
    reload.addEventListener("click", function() {location.reload()});
});

siteblock.night.isBlocked("http://" + site).then(function(isBlocked) {
	if(!isBlocked) {
		if(params.get("redirect")) {
			location.href = params.get("redirect");
		} else {
			location.href = "http://" + site;
		}
	}
});