var ext = window.browser || chrome;

ext.storage.local.set({
	time: {
		start: Date.now(),
		lastStart: Date.now(),
		duration: 0
	}
});

ext.idle.onStateChanged.addListener(function(state) {
	if(state != "active") {
		browser.storage.local.get("time", function(res) {
			var time = res.time;
			time.duration += Math.abs(Date.now() - time.lastStart);
			time.lastStart = null;
			browser.storage.local.set({time:time});
		});
	} else { // state is active
		browser.storage.local.get("time", function(res) {
			var time = res.time;
			time.lastStart = Date.now();
			browser.storage.local.set({time:time});
		});
	}
});

ext.contextMenus.create({
	id: "block-site",
	title: "Block this site",
	contexts: ["all"],
	checked: false,
	type: "checkbox"
});

ext.contextMenus.onClicked.addListener(function(info, tab) {
	if(info.checked) {
		siteblock.block.block(tab.url);
	} else {
		siteblock.block.unblock("http://" + decodeURI(new URLSearchParams(new URL(tab.url).search).get("site")));
	}
	ext.tabs.reload();
});

function setTabBlocked(tabId, host, redirect, text) {
  	ext.tabs.update(tabId, {url: ext.runtime.getURL("/inject.html?site=" + encodeURI(host) + "&redirect=" + encodeURI(redirect) +
		(text ? "&text=" + encodeURI(text) : ""))});
}

/* Tabs */
function tabListener(tabId, _, tab) {
    /* Update context menu */
	var title, host="";
	try {
		if(tab.url.indexOf(ext.runtime.getURL("/")) === 0) {
			host = decodeURI(new URLSearchParams(new URL(tab.url).search).get("site"));
		} else {
			host = new URL(tab.url).host;
			if(host == "")
				throw null;
		}
		title = "Block " + host;
	} catch(err) {
		console.log("Error: ", err);
		title = "Block this site";
	}
	ext.contextMenus.update("block-site",{
		title: title,
		checked: false
	});

	siteblock.night.isBlocked("http://" + host).then(function(isBlocked) {
		if(isBlocked) {
				if(tab.url.indexOf(ext.runtime.getURL("/")) === -1 && tab.url.indexOf("about:") === -1 && tab.url.indexOf("chrome://") === -1)
					siteblock.night.isNight("http:// " + host).then(function(isNight) {
						setTabBlocked(tab.id, host, tab.url, isNight ? "It's night" : null);
					});
				ext.contextMenus.update("block-site", {
					title: title,
					checked: true
				});
		} else {
			ext.contextMenus.update("block-site", {
				title: title,
				checked: false });
		}
	});
}

ext.tabs.onUpdated.addListener(tabListener);
ext.tabs.onActivated.addListener(function(activeInfo) {
	var tabId = activeInfo.tabId;
	if(window.browser)
		browser.tabs.get(tabId).then(function(tab) {
			tabListener(tabId, null, tab);
		});
	else
		chrome.tabs.get(tabId, function(tab) { tabListener(tabId, null, tab); });
});
