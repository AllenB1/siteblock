window.addEventListener("load", function() {
	if(navigator.userAgent.indexOf("Chrome") !== -1) {
		document.body.style.background = "#EEE";
	}

    var from = document.getElementById("from");
    var to = document.getElementById("to");

    function loadNight() {
        siteblock.night.get().then(function(night) {
            if(night != null) {
                from.value = night.from;
                to.value = night.to;
            }
        });
    }    
    loadNight();

    function saveNight() {
        if(from.value.length && to.value.length) {
            siteblock.night.set(from.value, to.value);
        }
				else if(!from.value.length && !to.value.length) {
					siteblock.night.remove();
				}
    }
    from.addEventListener("input", saveNight);
    to.addEventListener("input", saveNight);
});
