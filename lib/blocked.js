var siteblock = self.siteblock || {};
(function() {
    function localDo(arg, str) {
		if(self.browser) {
			return browser.storage.local[str](arg);
		} else if(self.chrome) {
			return new Promise(function(resolve) {
				chrome.storage.local[str](arg, function(x) {
                    resolve(x);
                });
			});
		} else {
			throw new Error("Neither self.browser nor self.chrome is an object");
		}
	}

	function localSet(arg) {
		return localDo(arg, 'set');
	}

	function localGet(arg) {
		return localDo(arg, 'get');
	}

    siteblock.block = (function() {
	    function listBlocked() {
		    return localGet('blocked').then(function(res) {
			    if(res.blocked instanceof Array) {
				    return res.blocked;
			    } else {
				    return [];
			    }
		    }).then(function(res) {
                return res;
            });
	    }

        function hostFromUrl(url) {
		    try {
			    var urlo = new URL(url);
			    return urlo.host;
		    } catch(err) {
                return "";
		    }
        }

	    function blockHost(host) {
            if(!host.length)
                return Promise.resolve();
		    return listBlocked().then(function(sites) {
			    sites.push(host);
			    return localSet({blocked:sites});
		    });
	    };
	    function block(url) {
		    return blockHost(hostFromUrl(url));
	    }

	    function unblockHost(host) {
		    return listBlocked().then(function(sites) {
			    var i;
			    while((i = sites.indexOf(host)) !== -1) {
				    sites.splice(i, 1);
			    }
			    return localSet({blocked: sites});
		    });
	    }

	    function unblock(url) {
		    return unblockHost(hostFromUrl(url));
	    }

	    // if blocked, unblock, otherwise block
	    // returns whether the site is blocked
	    function toggle(url) {
		    return isBlocked(url).then(function(is){
			    if(is) {
				    return unblock(url).then(function() { return false; });
			    } else {
				    return block(url).then(function() { return true; });
			    }
		    });
	    }

	    // non-mutating
	    function isBlockedHost(host) {
            if(!host.length)
                return Promise.resolve(false);
		    return listBlocked().then(function(sites) {		
			    return sites.indexOf(host) !== -1;
		    });
	    }
	    function isBlocked(text) {
		    return isBlockedHost(hostFromUrl(text));
	    }

	    return {block:block, isBlocked: isBlocked, list: listBlocked, unblock:unblock, toggle:toggle};
    })();

    siteblock.night = (function() {
        function isNight() {
            return getNightAsDates().then(function(night) {
                if(night === null)
                    return false;
                return night.from.getTime() <= Date.now() && Date.now() <= night.to.getTime();
            });
        }
        function isBlocked(site) {
            if(site.indexOf("about:") !== -1 || site.indexOf("chrome://") !== -1) {
                return Promise.resolve(false);
            }

            return isNight().then(function(isNight) {
                 return isNight ? true : siteblock.block.isBlocked(site);
            });
        }
        function getNight() {
            return localGet("night").then(function(vals) {
                if(!(vals.night instanceof Array) || vals.night.length < 2)
                    return null;
                else
                    return {from: vals.night[0], to: vals.night[1]};
            });
        }

        function getNightAsDates() {
            return getNight().then(function(night) {
                if(night == null)
                    return null;
                else {
                    var fromVals = night.from.split(":");
                    var toVals = night.to.split(":");

                    if(fromVals.length < 2 || toVals.length < 2)
                        return null;

                    var fromDate = new Date();
                    fromDate.setHours(fromVals[0] | 0);
                    fromDate.setMinutes(fromVals[1] | 0);
                    var toDate = new Date();         
                    toDate.setHours(fromVals[0] | 0);
                    toDate.setMinutes(fromVals[1] | 0);

                    if(toDate.getTime() <= fromDate.getTime()) {
                        toDate.setDate(toDate.getDate() + 1);
                    }

                    return {from: fromDate, to: toDate};
                }
            });
        }

        function setNight(from, to) {
            return localSet({night: [from, to]});
        }
        
        function setNightToDates(from, to) {
            return localSet({night: [
                from.getHours() + ":" + from.getMinutes(),
                to.getHours() + ":" + to.getMinutes()
            ]});
        }

				function removeNight() {
					return localDo("night", "remove");
				}

        return {isNight:isNight, isBlocked:isBlocked, set:setNight, setDates: setNightToDates, get:getNight, getAsDates: getNightAsDates, remove: removeNight};
    })();
})();
