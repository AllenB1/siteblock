// not used

siteblock = window.siteblock || {};

siteblock.alarms = {};
siteblock.alarms.addRecurring = function(name, time) {
	return new Promise(function(resolve, reject) {
		var callback = function(obj) {
			var arr_a = obj.alarms; /*Array of alarms*/
			if(!(arr_a instanceof Array)) {
				arr_a = [];
			}
			var alarm = {
				name: name || "Alarm",
				time: time
			};
			arr_a.push(alarm);
			console.log("time in addRecurring: " + alarm.time);
			if(window.browser)
				browser.storage.local.set({alarms: arr_a}).then(function() { resolve(alarm); });
			else
				chrome.storage.local.set({alarms: arr_a}, function() { resolve(alarm); });
		}
		if(window.browser)
	        browser.storage.local.get("alarms").then(callback).catch(reject);
	    else
	        chrome.storage.local.get("alarms", callback).catch(reject);
	});
};

siteblock.alarms.list = function() {
	return new Promise(function(resolve, reject) {
		var callback = function(obj) {
			if(obj.alarms instanceof Array) {
				obj.alarms.forEach(function(alarm, index) {
					if(typeof alarm.name !== "string" || alarm.name.trim() === "")
						obj.alarms[index].name = "Alarm";
				});
				resolve(obj.alarms);
			} else
				resolve([]);
		};
		if(window.browser)
			browser.storage.local.get("alarms").then(callback);
		else
			chrome.storage.local.get("alarms", callback);
	});
};

// returns {hour, min, sec}
siteblock.alarms.timeAsObject = function(alarm) {
	// hh:mm ss [optional ss]
	if(typeof alarm.time !== "string")
		return;
	var time_array = alarm.time.split(":");
	var hour = Number(time_array[0]);
	hour = Number.isNaN(hour) ? 12 : hour;
	var minute_and_sec;
	if(time_array[1])
		minute_and_sec = time_array[1].split(" ");
	else
		minute_and_sec = [];
	var minute = Number(minute_and_sec[0])|| 0;
	var second = Number(minute_and_sec[1]) || 0;

	return {hour: hour, min: minute, sec: second};
}


setTimeout(function() {
	siteblock.alarms.list().then(function(alarms) {
		alarms.forEach(function(alarm) {
			var alarm_time = siteblock.alarms.timeAsObject(alarm);

			var now = new Date();

			if(now.getHours() === alarm_time.hour && now.getMinutes() === alarm_time.min) {
				if(typeof siteblock.alarms.onalarm === "function") {
					siteblock.alarms.onalarm();
					console.log("Alarm: " + alarm.name);
				}
			}
		});
	});
}, 10000);

